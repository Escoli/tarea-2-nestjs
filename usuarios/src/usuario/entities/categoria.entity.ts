import{Column, Entity, ManyToMany, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Categoria{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    descripcion:string;

}