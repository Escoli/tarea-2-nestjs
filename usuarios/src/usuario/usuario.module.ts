import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { UsuarioController } from './usuario.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from './entities/usuario.entity';
import { UsuarioRepository } from './usuario.repository';
import { TareaModule } from 'src/tarea/tarea.module';
import { CatmoduleModule } from 'src/catmodule/catmodule.module';
import { Perfil } from './entities/perfil.entity';
import { PerfilRepository } from './perfil.repository';
import { PerfilService } from './perfil.service';


@Module({
  imports: [TypeOrmModule.forFeature([Usuario, Perfil]), TareaModule, CatmoduleModule],
  controllers: [UsuarioController],
  providers: [UsuarioService, UsuarioRepository, PerfilService,PerfilRepository ],
})
export class UsuarioModule { }
/* implements NestModule {
configure(consumer: MiddlewareConsumer) {
    //consumer.apply(LoggerMiddleware).forRoutes('usuarios');     //1 forma
    consumer.apply(LoggerMiddleware).forRoutes({path: 'usuarios/listar', method: RequestMethod.GET});  //Segunda forma
 }
}
*/
