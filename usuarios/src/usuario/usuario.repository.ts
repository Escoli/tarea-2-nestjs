import { Inject, Injectable } from "@nestjs/common";
import { Usuario } from "./entities/usuario.entity";
import { DataSource, Repository } from "typeorm";
import { CreateUsuarioDto } from "./dto/create-usuario.dto";
import { UpdateUsuarioDto } from "./dto/update-usuario.dto";
import { PaginacionDto } from "./dto/paginacion.dto";


@Injectable()
export class UsuarioRepository {
  constructor(
    /*
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>*/
    private dataSource: DataSource) { }
  async crear(createUsuarioDto: CreateUsuarioDto) {
    // const usuario=new Usuario({...createUsuarioDto});
    //return this.usuarioRepository 
    return await this.dataSource.getRepository(Usuario).save(createUsuarioDto);
  }

  async buscarPorId(id: number) {
    const usuario = await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .where('usuario.id=:id', { id })
      .getOne()
    return usuario;
  }

  async buscarPorNombre(nombreUsuario: string): Promise<Usuario> {
    console.log('nombre usuario ingresado', nombreUsuario);
    const usuario = await this.dataSource.manager
      .createQueryBuilder(Usuario, 'usuario')
      .where('usuario.nombreUsuario=:nombre', { nombre: nombreUsuario })
      .getOne();
    return usuario;
  }

  async listar() {
    const usuarios = this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .getMany();
    return usuarios;
  }

  async actualizar(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    await this.dataSource
      .createQueryBuilder()
      .update(Usuario)
      .set({ ...UpdateUsuarioDto })
      .where('id=:id', { id })
      .execute();
  }

  async eliminar(id: number) {
    await this.dataSource
      .createQueryBuilder()
      .delete()
      .from(Usuario)
      .where('id=:id', { id })
      .execute();
  }

  async listarTareas(id: number, paginacion: PaginacionDto) {
    const { limite, orden, estado } = paginacion;
    const tareas = this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.tarea', 'tarea')
      .leftJoinAndSelect('usuario.perfil', 'perfil')
      .select([
        'usuario.nombre',
        'perfil.foto',
        'tarea.titulo',
        'tarea.descripcion',
        'tarea.estado'])
      .where('tarea.idUsuario=:id', { id })
      .take(limite);
    if (orden) {
      tareas.orderBy('tarea.titulo', orden);
    }

    if (estado) {
      tareas.andWhere('tarea.estado=:estado', { estado: estado });
    }

    return await tareas.getRawMany();
  }





}









//----para MOstrar  todos los datos por usuario
/*   async listarTareas(id: number) {
    const tareas = await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect ('usuario.tarea', 'tarea' )
      .select([
        'usuario.nombre',
        'tarea.titulo',
        'tarea.descripcion',
        'tarea.estado'])
      .where('tarea.idUsuario=:id', { id })
      .getMany();
    return tareas;
  } */



//    buscarPorId(id:number){

//const usuario=this.dataSource.getRepository;
//     return this.usuarioRepository.findOneBy({id});

// }


/*    async buscarPorNombre(nombreUsuario: string): Promise<Usuario>{
       return await this.usuarioRepository.findOne({where:{nombreUsuario}})
   } */
/*    async buscarPorNombre(nombreUsuario: string): Promise<Usuario>{
        return await this.dataSource
        .getRepository(Usuario)
        .createQueryBuilder('usuario')
        .where('usuario.nombreUsuario =:nombre',{nombre:nombreUsuario})
         .getOne();
    } */


//   listar(){
//  //const usuario=this.dataSource.getRepository;
//   return this.usuarioRepository.find();
//}

//actualizar(id:number, updateUsuarioDto: UpdateUsuarioDto){
//  //const usuario=this.dataSource.getRepository;
//return this.usuarioRepository.update(id, updateUsuarioDto)
//    }
//  eliminar(id: number){
//    return this.usuarioRepository.delete(id);
//}
