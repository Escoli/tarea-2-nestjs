import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';
//Describe un conjunto de reglas o test
describe('UsuarioController', () => {
  let controller: UsuarioController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuarioController],
      providers: [UsuarioService],
    }).compile();

    controller = module.get<UsuarioController>(UsuarioController);
  });
//para ejecutar las pruebas
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
