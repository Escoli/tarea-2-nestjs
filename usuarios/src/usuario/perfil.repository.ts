import { Injectable } from "@nestjs/common";
import { DataSource } from "typeorm";
import { PerfilDto } from "./dto/perfil.dto";
import { Perfil } from "./entities/perfil.entity";

@Injectable()
export class PerfilRepository{
    constructor(private dataSource:DataSource){}
    async crear(perfilDto:PerfilDto){
        return await this.dataSource.getRepository(Perfil).save(perfilDto);
    }

}