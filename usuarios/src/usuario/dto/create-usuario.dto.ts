import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, MinLength, IsAlphanumeric, minLength, isNumber, IsNumber } from "class-validator";

export class CreateUsuarioDto {
 
    @IsNumber()
    @IsNotEmpty()
    id: number;


    //inicio para documetación ejemplo
    @ApiProperty({
        example: "Lidia",
        required: true,
    }) // fin documentación ejemplo
    @IsString()
    @IsNotEmpty()
    @MinLength(5,
        {message: 'El nombre de usuario debería tener al menos 5 caracteres',
    })
    nombre: string;

    //inicio para documetación ejemplo
    @ApiProperty({
        example: "Escoli",
        required: true,
    }) // fin documentación ejemplo
    @IsString()
    @IsNotEmpty()
    @MinLength(3,
        {message: 'El nickname de usuario debería tener al menos 3 caracteres',
    })
    @IsAlphanumeric(null, {message: 'Solo se permiten numeros y  letras'})
    nombreUsuario: string;

    //inicio para documetación ejemplo
    @ApiProperty({
        example: "lidia24@gmail.com",
        required: true,
    }) // fin documentación ejemplo
    @IsString()
    @IsNotEmpty()
    @IsEmail(null, {message: 'Ingrese un email válido'})    
    email: string;

       //inicio para documetación ejemplo
    @ApiProperty({
        example: "yoPuedo.24.*",
        required: true,
    }) // fin documentación ejemplo
    @IsString()
    @IsNotEmpty()
    password: string;

}
