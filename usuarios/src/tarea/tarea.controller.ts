import { ConflictException, Controller } from '@nestjs/common';
import { CreateUsuarioDto } from 'src/usuario/dto/create-usuario.dto';
import { Tarea } from 'src/usuario/entities/tarea.entity.ts';

@Controller('tarea')
export class TareaController {
    usuarioRepository: any;
    async create(createUsuarioDto: CreateUsuarioDto): Promise<Tarea> {
        return await this.usuarioRepository.crear(CreateUsuarioDto);
    }
}



