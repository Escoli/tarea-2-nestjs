import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //Para realizar la documentación
  const options=new DocumentBuilder()
  .setTitle('Autenticación de usuarios')
  .setDescription('API para autenticar usuarios')
  .setVersion('1.0')
  .addServer('http://localhost:3000')
  .addTag('Auth')
  .addBearerAuth({
    description:'Autorización depo  defecto de Jwt',
    type:'http',
    scheme:'bearer',
    bearerFormat:'JWT',
  })
  .build()
  const document=SwaggerModule.createDocument(app,options)
  SwaggerModule.setup('api-docs',app, document)
  await app.listen(3000);
}
bootstrap();
