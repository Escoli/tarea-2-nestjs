import { MiddlewareConsumer, Module, NestModule, forwardRef } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioModule } from './usuario/usuario.module';
import { Usuario } from './usuario/entities/usuario.entity';
import { LoggerMiddleware } from './middleware/logger.middleware';
import { AuthModule } from './auth/auth.module';
import { Perfil } from './usuario/entities/perfil.entity';
import { Tarea } from './usuario/entities/tarea.entity.ts';
import { Categoria } from './usuario/entities/categoria.entity';
import { TareaModule } from './tarea/tarea.module';
import { CatmoduleModule } from './catmodule/catmodule.module';

@Module({
  imports: [
    //configuración de la conexion a la base de datos
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'lepostgr3',
      database: 'usuarios',
      entities: [Usuario, Perfil, Tarea, Categoria],
      synchronize: true,
      logging: true,
    }),
    UsuarioModule,
    AuthModule,
    TareaModule,
    CatmoduleModule,
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
